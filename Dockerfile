FROM ubuntu:20.04

ENV LANG=en_US.UTF-8
RUN apt update && apt install -y python3-pip python3-setuptools python3-urllib3 pipenv git
COPY src /app
COPY Pipfile /app
COPY Pipfile.lock /app
WORKDIR /app
RUN git clone https://gitlab.com/evilcouncil/python_logcollector.git
RUN pipenv install
CMD pipenv run python main.py
