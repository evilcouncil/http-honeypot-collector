create table raw_events(
    event_id INT NOT NULL AUTO_INCREMENT,
    date_recorded INT NOT NULL,
    date_processed INT NOT NULL,
    remote_host VARCHAR(128) NOT NULL,
    remote_port INT NOT NULL,
    method VARCHAR(8) NOT NULL,
    url VARCHAR(512) NOT NULL,
    destination VARCHAR(128) NOT NULL,
    sensor_name VARCHAR(128) NOT NULL,
    user_agent VARCHAR(256) NOT NULL,
    headers JSON,
    post_value JSON,
    PRIMARY KEY ( event_id )
);
