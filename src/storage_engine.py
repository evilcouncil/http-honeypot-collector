"""Store log events."""

import mysql.connector
import httphoneypotevent
import time
import json


class StorageEngine:
    """Takes emails and stores them."""

    def __init__(self, host: str, username: str, password: str, database: str):
        self._host = host
        self._username = username
        self._password = password
        self._database = database
        self._conn = None

    def __enter__(self):
        """Connects to database."""
        self._conn = mysql.connector.connect(
            host=self._host,
            username=self._username,
            password=self._password,
            database=self._database,
        )

    def __exit__(self, error_type, value, traceback):
        """Close connections."""
        self._conn.close()

    def store(self, event: httphoneypotevent.HttpHoneypotEvent):
        """Stores messages."""
        cursor = self._conn.cursor()
        sql_statement = (
            "insert  into raw_events(date_recorded, date_processed,"
            + "remote_host, remote_port,"
            + "method, url, destination, sensor_name, user_agent,"
            + "headers, post_value) "
            + "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        )
        cursor.execute(
            sql_statement,
            (
                int(time.time()),
                event.event_time,
                event.remote_host,
                event.remote_port,
                event.method,
                event.url,
                event.destination,
                event.sensor_name,
                event.user_agent,
                json.dumps(event.headers),
                json.dumps(event.post_values),
            ),
        )
        self._conn.commit()
