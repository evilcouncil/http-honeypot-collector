import json


class HttpHoneypotEvent:
    def __init__(self, event_line):
        data = json.loads(event_line)
        self._remote_host = data["remotehost"].split(":")[0]
        self._remote_port = int(data["remotehost"].split(":")[1])
        self._method = data["method"]
        self._url = data["url"]
        self._destination = data["destination"]
        self._event_time = data["eventtime"]
        self._sensor_name = data["sensorname"]
        self._headers = data["headers"]
        self._user_agent = data["headers"].get("User-Agent", "NOAGENT")
        if isinstance(self._user_agent, list):
            self._user_agent = self._user_agent[0]
        self._post_values = data["postvalues"]

    @property
    def remote_host(self) -> str:
        return self._remote_host

    @property
    def remote_port(self) -> int:
        return self._remote_port

    @property
    def method(self) -> str:
        return self._method

    @property
    def url(self) -> str:
        return self._url

    @property
    def destination(self) -> str:
        return self._destination

    @property
    def event_time(self) -> int:
        return self._event_time

    @property
    def sensor_name(self) -> str:
        return self._sensor_name

    @property
    def headers(self):
        return self._headers

    @property
    def user_agent(self) -> str:
        return self._user_agent

    @property
    def post_values(self):
        return self._post_values
