from python_logcollector import logcollector
import httphoneypotevent
import storage_engine
import dotenv
import os
import constants


def process_logfile(logfile: str, storage_eng: storage_engine.StorageEngine):
    with storage_eng:
        for line in logfile.split("\n"):
            if line:
                event = httphoneypotevent.HttpHoneypotEvent(line)
                storage_eng.store(event)


def main():
    dotenv.load_dotenv()
    logserve_server = os.environ[constants.LOGSERVE_SERVER]
    logserve_port = os.environ[constants.LOGSERVE_PORT]
    collect_cert = os.environ[constants.COLLECTOR_CERT]
    collect_key = os.environ[constants.COLLECTOR_KEY]
    ca_cert = os.environ[constants.CA_CERT]
    mysql_server = os.environ[constants.MYSQL_SERVER]
    mysql_user = os.environ[constants.MYSQL_USER]
    mysql_pass = os.environ[constants.MYSQL_PASS]
    mysql_db = os.environ[constants.MYSQL_DB]

    collect = logcollector.LogCollector(
        logserve_server,
        logserve_port,
        collect_cert,
        collect_key,
        ca_cert,
    )
    files = collect.list_files()

    storage = storage_engine.StorageEngine(
        mysql_server,
        mysql_user,
        mysql_pass,
        mysql_db,
    )

    try:
        for current in files["CurrentLogFiles"]:
            print(f"downloading {current}")
            logfile = collect.get_file(current)
            print(f"storing {current}")
            process_logfile(logfile, storage)
            print(f"deleting{current}")
            collect.delete_file(current)
    except Exception as e:
        print(e)


if __name__ == "__main__":
    main()
